<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function blogcategories()
    {
        return $this->belongsToMany('App\Blogcategory');
    }

    public function blogtags()
    {
    	return $this->belongsToMany('App\Blogtag');
    }
    public function postsphotos()
    {
        return $this->hasMany('App\PostsPhoto');
    }
    public function pics(){
        return $this->hasMany(Pic::class);
    }

}
