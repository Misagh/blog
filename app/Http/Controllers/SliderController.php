<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Session;
use Image;
use File;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::paginate(5);
        return view('slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'title'         => 'required|min:2|max:255',
            'link'          => 'max:255',
        ));
        //store in database
        $slider = new Slider;
        $slider->title = $request->title;
        $slider->body = $request->body;
        $slider->link = $request->link;

        if ($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('slider/' . $filename);
            Image::make($image)->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            $slider->image = $filename;
          }

        $slider->save();
        Session::flash('success', 'اسلایدر ساخته شد');
        return redirect()->route('sliders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Slider::find($id);
        return view('slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider = Slider::find($id);
        $this->validate($request, array(
            'title'         => 'required|min:2|max:255',
            'link'          => 'max:255',
        ));

        $slider->title = $request->input('title');
        $slider->body = $request->input('body');
        $slider->link = $request->input('link');

        if($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('slider/' . $filename);
            Image::make($image)->resize(50, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);

            $oldFilename = $slider->image;

            $slider->image = $filename;

            File::delete('slider/' .$oldFilename);
        }

        $slider->save();
        Session::flash('success', 'اسلایدر بروزرسانی شد');
        return redirect()->route('sliders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::find($id);
        File::delete('slider/' .$slider->image);
        $slider->delete();

        Session::flash('success', 'اسلایدر شما حذف شد');
        return redirect()->route('sliders.index');
    }
}
