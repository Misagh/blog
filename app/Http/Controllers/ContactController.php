<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use Session;

class ContactController extends Controller
{
    public function getContact() {
		return view('pages.contact');
	}
	public function postContact(Request $request) {
		$this->validate($request, [
			'name' => 'required',
			'email' => 'required|email',
			'subject' => 'min:3',
			'message' => 'min:10']);

		$data = array(
			'name' => $request->name,
			'email' => $request->email,
			'subject' => $request->subject,
			'bodyMessage' => $request->message
			);

		Mail::send('emails.contact', $data, function($message) use ($data){
			$message->from($data['email']);
			$message->to('misagh.90@gmail.com');
			$message->subject($data['subject']);
		});

		Session::flash('success', 'پیام شما ارسال شد');

        return redirect('/');

	}
}
