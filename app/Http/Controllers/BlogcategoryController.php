<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blogcategory;
use Session;

class BlogcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Blogcategory::all();
        return view('blogcategories.index')->withCategories($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'name' => 'required|max:255',
            'body' => 'min:5'
        ));
        $category = new Blogcategory;
        $category->name = $request->name;
        $category->body = $request->body;
        $category->save();

        Session::flash('success', 'دسته بندی جدید ایجاد شد');

        return redirect()->route('blogcategories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Blogcategory::find($id);
        return view('blogcategories.show')->withCategory($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Blogcategory::find($id);
        return view('blogcategories.edit')->withCategory($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Blogcategory::find($id);

        $this->validate($request, [
            'name' => 'required|max:255',
            'body' => 'min:5'
            ]);

        $category->name = $request->name;
        $category->body = $request->body;
        $category->save();

        Session::flash('success', 'تغییرات ایجاد شد');

        return redirect()->route('blogcategories.show', $category->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Blogcategory::find($id);
        $category->posts()->detach();

        $category->delete();

        Session::flash('success', 'دسته بندی حذف شد');

        return redirect()->route('blogcategories.index');
    }
}
