<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Session;
use App\Blogcategory;
use App\Blogtag;
use Image;
use Storage;
use File;
use App\PostsPhoto;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(5);
        // return $posts;
        return view('posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blogcategories = Blogcategory::all();
        $blogtags = Blogtag::all();
        return view('posts.create')->withBlogcategories($blogcategories)->withBlogtags($blogtags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate data
        $this->validate($request, array(
            'title'         => 'required|min:2|max:255',
            'body'          => 'required|min:15',
            'slug'          => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
            'photo'         => 'image|mimes:jpeg,bmp,png,jpg|size:3072'
            // 'category_id'   => 'required|integer'
        ));
        //store in database
        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->slug = $request->slug;
        // $post->category_id = $request->category_id;

        if ($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->fit(200)->save($location);
            $post->image = $filename;
          }


        $post->save();
        $post->blogtags()->sync($request->blogtags, false);
        $post->blogcategories()->sync($request->blogcategories, false);

        if ($request->hasFile('photos')){
            foreach ($request->photos as $photo) {
            $filenameWithExt = $photo->getClientOriginalName();
            $filename = $filenameWithExt.time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($photo)->fit(200)->save($location);
            PostsPhoto::create([
                'post_id' => $post->id,
                'filename' => $filename
                ]);
                }
            }
        Session::flash('success', 'مطلب شما ایجاد شد');
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $photos =PostsPhoto::with('Post')->find($id);
        return view ('posts.show', compact('post', 'photos'));
        // return $post;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        $blogcategories = Blogcategory::all();
        $cats2 = array();
        foreach ($blogcategories as $blogcategory) {
            $cats2[$blogcategory->id] = $blogcategory->name;
        }
        $blogtags = Blogtag::all();
        $blogtags2 = array();
        foreach ($blogtags as $blogtag) {
            $blogtags2[$blogtag->id] = $blogtag->name;
        }
        return view('posts.edit')->withPost($post)->withBlogcategories($cats2)->withBlogtags($blogtags2);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if ($request->input('slug') == $post->slug) {
            $this->validate($request, array(
                'title' => 'required|min:2|max:255',
                'body'  => 'required|min:15',
                // 'category_id' => 'required|integer'
            ));
        } else {
        $this->validate($request, array(
            'title'         => 'required|min:2|max:255',
            'body'          => 'required|min:15',
            'slug'          => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
            // 'category_id' => 'required|integer'
        ));
    }

        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->slug = $request->input('slug');
        // $post->category_id = $request->input('category_id');

        if($request->hasFile('featured_img')) {
            $image = $request->file('featured_img');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->fit(200)->save($location);

            $oldFilename = $post->image;

            $post->image = $filename;

            File::delete('images/' .$oldFilename);
        }

        $post->save();
        if (isset($request->blogtags)) {
            $post->blogtags()->sync($request->blogtags);
        } else {
            $post->blogtags()->sync(array());
        }

        if (isset($request->blogcategories)) {
            $post->blogcategories()->sync($request->blogcategories);
        } else {
            $post->blogcategories()->sync(array());
        }

        if ($request->hasFile('photos')){
            // PostsPhoto::where('post_id', $id)->delete();
            foreach ($request->photos as $photo) {
            $filenameWithExt = $photo->getClientOriginalName();
            $filename = $filenameWithExt.time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($photo)->fit(200)->save($location);
            PostsPhoto::create([
                'post_id' => $post->id,
                'filename' => $filename
                ]);
                }
        }


        Session::flash('success', 'مطلب شما بروزرسانی شد');
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->blogtags()->detach();
        $post->blogcategories()->detach();
        $photos = PostsPhoto::where('post_id', $id)->get();
        foreach($photos as $photo){
            File::delete('images/' .$photo->filename);
        }
        PostsPhoto::where('post_id', $id)->delete();
        File::delete('images/' .$post->image);

        $post->delete();

        Session::flash('success', 'مطلب شما حذف شد');
        return redirect()->route('posts.index');
    }
}
