<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\Slider;

class PagesController extends Controller {

	public function index() {
        $posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
        $sliders = Slider::orderBy('updated_at', 'desc')->get();
		return view('pages.welcome' , compact('posts', 'sliders'));
	}

}
