<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blogtag extends Model
{
    public function posts()
    {
    	return $this->belongsToMany('App\Post');
    }
}
