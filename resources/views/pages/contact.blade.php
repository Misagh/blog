@extends('main')

@section('title', '| Contact')

@section('content')
        <div class="row">
            <div class="col-md-12">
                <h1>Contact Me</h1>
                <hr>
                <form action="{{ url('contact') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label name="name">نام شما:</label>
                        <input id="name" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label name="email">پست الکترونیک:</label>
                        <input id="email" name="email" class="form-control">
                    </div>
                    {{-- <div class="form-group">
                        <label name="phone">تلفن تماس:</label>
                        <input id="phone" name="phone" class="form-control">
                    </div> --}}

                    <div class="form-group">
                        <label name="subject">موضوع:</label>
                        <input id="subject" name="subject" class="form-control">
                    </div>

                    <div class="form-group">
                        <label name="message">پیام:</label>
                        <textarea id="message" name="message" class="form-control" placeholder="Type your message here..."></textarea>
                    </div>

                    <input type="submit" value="Send Message" class="btn btn-success">
                </form>
            </div>
        </div>
@endsection