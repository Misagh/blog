@extends('main')

@section('title', '| View Post')

@section('content')

	<div class="row">
		<div class="col-md-8">
			<h1>{{ $post->title }}</h1>

			<p class="lead">{!! $post->body !!}</p>

			<hr>

			<div class="tags">
				@foreach ($post->blogtags as $blogtag)
					<span class="label label-default"><a href="{{ route('blogtags.show', $blogtag->id) }}">{{ $blogtag->name }}</a></span>
				@endforeach
			</div>
		<img src="{{asset('/images/' . $post->image)}}" alt="">
		<hr>
        @foreach($post->postsphotos as $photo)
            <img src="{{asset('/images/' . $photo->filename)}}">
        @endforeach
		<hr>

			{{-- <div id="backend-comments" style="margin-top: 50px;">
				<h3>Comments <small>{{ $post->comments()->count() }} total</small></h3>

				<table class="table">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
							<th>Comment</th>
							<th width="70px"></th>
						</tr>
					</thead>

					<tbody>
						@foreach ($post->comments as $comment)
						<tr>
							<td>{{ $comment->name }}</td>
							<td>{{ $comment->email }}</td>
							<td>{{ $comment->comment }}</td>
							<td>
								<a href="{{ route('comments.edit', $comment->id) }}" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-pencil"></span></a>
								<a href="{{ route('comments.delete', $comment->id) }}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div> --}}
		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<label>Url:</label>
					<p><a href="{{ url('blog/'.$post->slug) }}">{{ url('blog/'.$post->slug) }}</a></p>
				</dl>

				<div class="tags">
					@foreach ($post->blogcategories as $blogcategory)
						<span class="label label-default"><a href="{{ route('blogcategories.show', $blogcategory->id) }}">{{ $blogcategory->name }}</a></span>
					@endforeach
				</div>

				{{-- <dl class="dl-horizontal">
					<label>دسته بندی:</label>
					<p><a href="{{ route('categories.show', $post->category->id) }}" class="btn btn-default btn-sm">{{ $post->category->name }}</a></p>
					<label>توضیح دسته بندی: </label>
					<p>{{ $post->category->body }}</p>
				</dl> --}}

				<dl class="dl-horizontal">
					<label>ایجاد شده در:</label>
					<p>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</p>
					<label>ایجاد شده در:</label>
					<p>{{ $post->created_at }}</p>
				</dl>

				<dl class="dl-horizontal">
					<label>آخرین بروزرسانی:</label>
					<p>{{ date('Y-m-d H:i:s', strtotime($post->updated_at)) }}</p>
				</dl>
				<hr>
				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('posts.edit', 'Edit', array($post->id), array('class' => 'btn btn-primary btn-block')) !!}
					</div>

					<div class="col-sm-6">
						{!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE']) !!}

							{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</div>
				</div>

				{{-- <div class="row">
					<div class="col-md-12">
						{{ Html::linkRoute('posts.index', '<< See All Posts', array(), ['class' => 'btn btn-default btn-block btn-h1-spacing']) }}
					</div>
				</div> --}}

			</div>
		</div>
	</div>

@endsection
