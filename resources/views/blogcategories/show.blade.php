@extends('main')

@section('title', "| $category->name ")

@section('content')

	<div class="row">
		<div class="col-md-8">
			<h1>{{ $category->name }} دسته بندی <small>{{ $category->posts()->count() }} مطلب</small></h1>
		</div>
		<div class="col-md-2">
			<a href="{{ route('blogcategories.edit', $category->id) }}" class="btn btn-primary pull-right btn-block" style="margin-top:20px;">Edit</a>
		</div>
		<div class="col-md-2">
			{{ Form::open(['route' => ['blogcategories.destroy', $category->id], 'method' => 'DELETE']) }}
				{{ Form::submit('Delete', ['class' => 'btn btn-danger btn-block', 'style' => 'margin-top:20px;']) }}
			{{ Form::close() }}
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>عنوان مطلب</th>
						<th>تگ ها</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					@foreach ($category->posts as $post)
					<tr>
						<th>{{ $post->id }}</th>
						<td>{{ $post->title }}</td>
						<td>@foreach ($post->blogcategories as $blogcategory)
								<span class="label label-default">{{ $blogcategory->name }}</span>
							@endforeach
							</td>
						<td><a href="{{ route('posts.show', $post->id ) }}" class="btn btn-default btn-xs">View</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

@endsection