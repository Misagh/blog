@extends('main')

@section('title', '| All Categories')

@section('content')

	<div class="row">
		<div class="col-md-8">
			<h1>دسته بندی</h1>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
                        <th>نام</th>
                        <th>توضیحات</th>
					</tr>
				</thead>

				<tbody>
					@foreach ($categories as $category)
					<tr>
						<th>{{ $category->id }}</th>
                        <td><a href="{{ route('blogcategories.show', $category->id) }}">{{ $category->name }}</a></td>
                        <td>{{ $category->body }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div> <!-- end of .col-md-8 -->

		<div class="col-md-3">
			<div class="well">
				{!! Form::open(['route' => 'blogcategories.store', 'method' => 'POST']) !!}
					<h2>دسته بندی جدید</h2>
					{{ Form::label('name', 'نام تگ') }}
                    {{ Form::text('name', null, ['class' => 'form-control']) }}

                    {{ Form::label('body', "محتوا") }}
				    {{ Form::textarea('body', null, array('class' => 'form-control')) }}

					{{ Form::submit('ایجاد دسته بندی جدید', ['class' => 'btn btn-primary btn-block btn-h1-spacing']) }}
				
				{!! Form::close() !!}
			</div>
		</div>
	</div>

@endsection