@extends('main')

@section('title', "| Edit Category")

@section('content')
	
	{{ Form::model($category, ['route' => ['blogcategories.update', $category->id], 'method' => "PUT"]) }}
		
		{{ Form::label('name', "Title:") }}
		{{ Form::text('name', null, ['class' => 'form-control']) }}

		{{ Form::label('body', "محتوا") }}
		{{ Form::textarea('body', null, array('class' => 'form-control')) }}

		{{ Form::submit('ذخیره تغییرات', ['class' => 'btn btn-success', 'style' => 'margin-top:20px;']) }}
	{{ Form::close() }}

@endsection