@extends('main')

@section('title', '| Sliders')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>اسلایدر</h1>
		</div>

		<div class="col-md-2">
			<a href="{{ route('sliders.create') }}" class="btn btn-lg btn-block btn-primary">ایجاد یک اسلایدر</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
                    <th>#</th>
                    <th>تصویر</th>
					<th>عنوان</th>
                    <th>محتوا</th>
                    <th>لینک</th>
					<th>تاریج ایجاد</th>
					<th></th>
				</thead>

				<tbody>

					@foreach ($sliders as $slider)

						<tr>
                            <th>{{ $slider->id }}</th>
                            <td><img src="{{asset('/slider/' . $slider->image)}}" alt=""></td>
							<td>{{ $slider->title }}</td>
                            <td>{{ substr(strip_tags($slider->body), 0, 50) }}{{ strlen(strip_tags($slider->body)) > 50 ? "..." : "" }}</td>
                            <td>{{ $slider->link }}</td>
							<td>{{ date('M j, Y', strtotime($slider->created_at)) }}</td>
							<td>
                                {{ Form::open(['route' => ['sliders.destroy', $slider->id], 'method' => 'DELETE']) }}
                                    {{ Form::submit('حذف', ['class' => 'btn btn-danger btn-block']) }}
                                {{ Form::close() }}
                                 <a href="{{ route('sliders.edit', $slider->id) }}" class="btn btn-default btn-sm">ویرایش</a>
                            </td>
						</tr>

                    @endforeach


				</tbody>
			</table>

			<div class="text-center">
				{!! $sliders->links(); !!}
			</div>
		</div>
	</div>

@stop
