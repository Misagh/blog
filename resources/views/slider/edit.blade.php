@extends('main')

@section('title', '| Edit Slider')

@section('stylesheets')

	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

	<script>
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: false
		});
	</script>

@endsection

@section('content')

	<div class="row">
		{!! Form::model($slider, ['route' => ['sliders.update', $slider->id], 'method' => 'PUT', 'files' => true]) !!}
		<div class="col-md-8">
			{{ Form::label('title', 'عنوان:') }}
			{{ Form::text('title', null, ["class" => 'form-control input-lg']) }}

			{{ Form::label('link', 'لینک ارجاع:', ['class' => 'form-spacing-top']) }}
			{{ Form::text('link', null, ['class' => 'form-control']) }}

			{{ Form::label('featured_img', 'تصویر') }}
			{{ Form::file('featured_img') }}

			{{ Form::label('body', "محتوا:", ['class' => 'form-spacing-top']) }}
			{{ Form::textarea('body', null, ['class' => 'form-control']) }}
		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>ایجاد شده در:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($slider->created_at)) }}</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>آخرین تغییرات:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($slider->updated_at)) }}</dd>
				</dl>
				<hr>
				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('sliders.index', 'لغو', array($slider->id), array('class' => 'btn btn-danger btn-block')) !!}
					</div>
					<div class="col-sm-6">
						{{ Form::submit('ذخیره تغییرات', ['class' => 'btn btn-success btn-block']) }}
					</div>
				</div>

			</div>
		</div>
		{!! Form::close() !!}
	</div>

@stop

@section('scripts')

	<script type="text/javascript">
		$('.select2-multi').select2();
	</script>

@endsection
