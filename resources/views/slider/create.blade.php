@extends('main')

@section('title', '| Create New Slider')

@section('stylesheets')

	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

	<script>
		tinymce.init({
			selector: 'textarea',
			plugins: 'link code',
			menubar: false
		});
	</script>

@endsection

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h1>ایجاد یک اسلاید</h1>
			<hr>
			{!! Form::open(array('route' => 'sliders.store', 'data-parsley-validate' => '', 'files' => true)) !!}
				{{ Form::label('title', 'عنوان') }}
				{{ Form::text('title', null, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}

				{{ Form::label('link', 'لینک ارجاع') }}
				{{ Form::text('link', null, array('class' => 'form-control')) }}

				{{ Form::label('featured_img', 'تصویر اسلایدر') }}
				{{ Form::file('featured_img') }}

				{{ Form::label('body', "محتوا") }}
				{{ Form::textarea('body', null, array('class' => 'form-control')) }}

				{{ Form::submit('ایجاد اسلایدر', array('class' => 'btn btn-success btn-lg btn-block', 'style' => 'margin-top: 20px;')) }}
			{!! Form::close() !!}
		</div>
	</div>

@endsection
