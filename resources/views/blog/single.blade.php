@extends('main')
@section('title', "| $post->title")

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1>{{ $post->title }}</h1>
            <img src="{{asset('/images/' . $post->image)}}" alt="">
            <p>{!! $post->body !!}</p>
            <hr>
            @foreach ($post->blogcategories as $blogcategory)
				<span class="label label-default"><a href="{{ route('blogcategories.show', $blogcategory->id) }}">{{ $blogcategory->name }}</a></span>
                <p> توضیح دسته بندی: {{ $blogcategory->body }}</p>
            @endforeach
            <hr>
            @foreach ($post->blogtags as $blogtag)
				<span class="label label-default"><a href="{{ route('blogtags.show', $blogtag->id) }}">{{ $blogtag->name }}</a></span>
            @endforeach
        </div>
    </div>


@endsection