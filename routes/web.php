<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['web']], function(){
    Route::get('/blog/{slug}', ['as' => 'blog.single', 'uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+');
    Route::get('blog', ['uses' => 'BlogController@getIndex', 'as' => 'blog.index']);
    Route::get('/', 'PagesController@index');
    Route::get('/contact', 'ContactController@getContact');
    Route::post('/contact', 'ContactController@postContact');
    Route::resource('/posts', 'PostController');
    Route::resource('/blogcategories', 'BlogcategoryController', ['except' => ['create']]);
    Route::resource('/blogtags', 'BlogtagController', ['except' => ['create']]);
    Route::resource('/sliders', 'SliderController');
});
